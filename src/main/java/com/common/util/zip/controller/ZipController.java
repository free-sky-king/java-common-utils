package com.common.util.zip.controller;

import com.common.util.zip.ZipUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

/**
 * @author
 * @Describe 功能描述
 * @date
 */
@RestController
@Slf4j(topic = "ZipController")
public class ZipController {

    String srcDir = "C:\\Users\\kinson\\Desktop\\zipTest";
    String desDir = "C:\\Users\\kinson\\Desktop\\unzip\\";

    String desZipFilename = "C:\\Users\\kinson\\Desktop\\zipTest\\dest.zip";

    String desZipFilePath = "C:\\Users\\kinson\\Desktop\\";

    @RequestMapping(value = "toZip")
    public void toZip(HttpServletRequest request, HttpServletResponse response, String type) throws IOException {
        if (StringUtils.isEmpty(type)) {
            // 浏览器响应返回
            response.reset();
            // 设置response的header，response为HttpServletResponse接收输出流
            response.setContentType("application/zip");
            String zipFileName = "zipFile-" + LocalDateTime.now().format(DateTimeFormatter.ofPattern(
                    "yyyyMMddHHmmss")) + ".zip";
            response.setHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode(zipFileName, "UTF-8"
            ));
            ZipUtils.toZip(srcDir, response.getOutputStream(), true);
        } else if (StringUtils.equals(type, "2")) {
            // 浏览器响应返回2
            File file = new File(srcDir);
            if (file.isDirectory() && file.listFiles().length > 0) {
                File[] files = file.listFiles();
                List<File> fileList = Arrays.asList(files);
                response.reset();
                // 设置response的header，response为HttpServletResponse接收输出流
                response.setContentType("application/zip");
                String zipFileName = "zipFile2-" + LocalDateTime.now().format(DateTimeFormatter.ofPattern(
                        "yyyyMMddHHmmss")) + ".zip";
                response.setHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode(zipFileName, "UTF-8"
                ));
                ZipUtils.toZip(fileList, response.getOutputStream());
            }
        } else if (StringUtils.equals(type, "3")) {
            // 指定目录
            File file = new File(srcDir);
            File desZipFile = new File(desZipFilename);
            if (file.isDirectory() && file.listFiles().length > 0) {
                File[] files = file.listFiles();
                ZipUtils.toZip(files, desZipFile);
            }
        } else {
            log.info("type is {}", type);
        }

    }

    @RequestMapping(value = "unZip")
    public void unZip(String zipFilename) throws IOException {
        Assert.isTrue(StringUtils.isNotEmpty(StringUtils.trim(zipFilename)));
        File file = new File(desZipFilePath + zipFilename + ".zip");
        ZipUtils.unZipFiles(file, desDir);
    }
}
