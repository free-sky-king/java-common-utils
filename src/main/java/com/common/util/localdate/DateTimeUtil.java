package com.common.util.localdate;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.Assert;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.TimeZone;

/**
 * 文件描述 时间日期工具类
 *
 * @author
 * @date
 */
@Slf4j(topic = "日期时间工具")
public class DateTimeUtil {

    /**
     * 时间格式化
     */
    public final static DateTimeFormatter FORMATTER_DATETIME_YMDHMSS = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ssSSS");
    public final static DateTimeFormatter FORMATTER_DATETIME_YMDHMS_YES = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    public final static DateTimeFormatter FORMATTER_DATETIME_YMDHMS_NO = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
    public final static DateTimeFormatter FORMATTER_DATETIME_YMD_YES = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public final static DateTimeFormatter FORMATTER_DATETIME_YMD = DateTimeFormatter.ofPattern("yyyyMMdd");
    public final static DateTimeFormatter FORMATTER_DATETIME_YM = DateTimeFormatter.ofPattern("yyyy-MM");
    public final static DateTimeFormatter FORMATTER_DATETIME_HMS = DateTimeFormatter.ofPattern("HHmmss");

    /**
     * 格式化日期
     * yyyy-MM-dd HH:mm:ss
     *
     * @return String
     */
    public static String formatDateTime(LocalDateTime date, DateTimeFormatter formatter) {
        String dateStr = "";
        try {
            dateStr = formatter.format(date);
        } catch (Exception e) {
            log.error("[DateTimeUtil] formatDateTime 异常,{}", e);
            throw new RuntimeException("日期格式化出错");
        }

        return dateStr;
    }

    /**
     * 根据当前时间生成临时文件名（年月日时分秒）
     *
     * @param date      当前时间
     * @param formatter 时间格式化
     * @return String
     */
    public static String tmpDirNameGenByCurDate(LocalDateTime date, DateTimeFormatter formatter) {
        Assert.notNull(formatter, "格式化类型不能为空");
        try {
            return formatter.format(date);
        } catch (Exception e) {
            log.error("[DateTimeUtil] tmpDirNameGenByCurDate 异常,{}", e);
            throw new RuntimeException("由时间生成临时文件名出错");
        }
    }

    /**
     * 时间转换为时间戳
     *
     * @param date
     * @return long
     */
    public static long date2Timestamp(LocalDateTime date) {
        try {
            return date.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
        } catch (Exception e) {
            log.error("[DateTimeUtil] getTimestamp 异常,{}", e);
            throw new RuntimeException("获取时间戳出错");
        }
    }

    /**
     * 时间戳转换为时间
     *
     * @param timestamp 时间戳
     * @return LocalDateTime
     */
    public static LocalDateTime timestamp2Date(long timestamp) {
        try {
            return LocalDateTime.ofInstant(Instant.ofEpochMilli(timestamp), TimeZone.getDefault().toZoneId());
        } catch (Exception e) {
            log.error("[DateTimeUtil] getTimestamp 异常,{}", e);
            throw new RuntimeException("获取时间戳出错");
        }
    }
}
