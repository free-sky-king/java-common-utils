# 1、SFTP java api
使用jsch实现sftp连接远程服务API操作

* pom依赖

     
     <!-- sftp -->
     <dependency>
         <groupId>com.jcraft</groupId>
         <artifactId>jsch</artifactId>
         <version>0.1.55</version>
     </dependency>

# 2.使用apache pool2实现sftp连接池管理
* pom依赖


     <!-- 连接池 -->
     <dependency>
         <groupId>org.apache.commons</groupId>
         <artifactId>commons-pool2</artifactId>
     </dependency>

* yml配置使用


     sftp-client:
       pool:
         max-total: 20
         max-idle: 10
         min-idle: 5
         lifo: true
         fairness: false
         max-wait-millis: 5000
         min-evictable-idle-time-millis: -1
         evictor-shutdown-timeout-millis: 10000
         soft-min-evictable-idle-time-millis: 1800000
         num-tests-per-eviction-run: 3
         test-on-create: false
         test-on-borrow: true
         test-on-return: false
         test-while-idle: true
         time-between-eviction-runs-millis: 600000
         block-when-exhausted: true
         jmx-enabled: false
         jmx-name-prefix: pool
         jmx-name-base: sftp
       abandoned:
         remove-abandoned-on-borrow: true
         remove-abandoned-on-maintenance: true
         remove-abandoned-timeout: 300
         log-abandoned: false
         require-full-stack-trace: false
         use-usage-tracking: false

