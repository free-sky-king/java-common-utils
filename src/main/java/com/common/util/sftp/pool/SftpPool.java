package com.common.util.sftp.pool;

import com.common.util.sftp.SftpUtil;
import org.apache.commons.pool2.PooledObjectFactory;
import org.apache.commons.pool2.impl.AbandonedConfig;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

/**
 * @author
 * @Describe sftp连接池
 * @date
 */
public class SftpPool extends GenericObjectPool<SftpUtil> {

    /**
     * Creates a new <code>GenericObjectPool</code> that tracks and destroys
     * objects that are checked out, but never returned to the pool.
     * 创建一个{@link GenericObjectPool}对象池，跟踪使用后未返回给对象池的对象，防止对象泄漏。
     *
     * @param factory         The object factory to be used to create object instances
     *                        used by this pool
     *                        对象工厂
     * @param config          The base pool configuration to use for this pool instance.
     *                        The configuration is used by value. Subsequent changes to
     *                        the configuration object will not be reflected in the
     *                        pool.
     *                        对象池配置
     * @param abandonedConfig Configuration for abandoned object identification
     *                        废弃对象跟踪配置
     */
    public SftpPool(PooledObjectFactory<SftpUtil> factory, GenericObjectPoolConfig<SftpUtil> config, AbandonedConfig abandonedConfig) {
        super(factory, config, abandonedConfig);
    }
}
