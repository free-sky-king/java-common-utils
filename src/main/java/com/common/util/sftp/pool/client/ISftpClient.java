package com.common.util.sftp.pool.client;

import com.common.util.sftp.SftpUtil;

/**
 * @author
 * @Describe 功能描述
 * @date
 */
public interface ISftpClient {

    /**
     * 获取sftp连接，执行handler的操作
     *
     * @param handler 具体的sftp操作
     */
    void open(Handler handler);

    interface Handler {

        /**
         * 执行sftp操作
         *
         * @param sftp Sftp实例
         * @throws Exception sftp操作可能抛出的任何异常
         * @see SftpUtil
         */
        void doHandle(SftpUtil sftp) throws Exception;
    }
}
