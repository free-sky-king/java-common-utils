package com.common.util.sftp.pool.autoconfiguration;

import com.common.util.sftp.pool.SftpFactory;
import com.common.util.sftp.pool.SftpPool;
import com.common.util.sftp.pool.client.ISftpClient;
import com.common.util.sftp.pool.client.MultipleSftpClient;
import com.common.util.sftp.pool.client.SftpClient;
import com.common.util.sftp.pool.config.SftpAbandonedConfig;
import com.common.util.sftp.pool.config.SftpPoolConfig;
import org.apache.commons.pool2.impl.DefaultEvictionPolicy;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.PrintWriter;
import java.util.Map;

/**
 * @author
 * @Describe sftp连接池自动配置
 * @date
 */
@Configuration
@ConditionalOnClass(SftpPool.class)
@EnableConfigurationProperties(SftpClientProperties.class)
public class SftpClientAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public ISftpClient sftpClient(SftpClientProperties sftpClientProperties) {
        // 多个连接
        if (sftpClientProperties.isMultiple()) {
            MultipleSftpClient multipleSftpClient = new MultipleSftpClient();
            Map<String, SftpClientProperties> clientProperties = sftpClientProperties.getClients();
            clientProperties.forEach((name, properties) -> {
                SftpFactory sftpFactory = createSftpFactory(properties);
                SftpPoolConfig sftpPoolConfig = createSftpPoolConfig(properties);
                SftpAbandonedConfig sftpAbandonedConfig = createSftpAbandonedConfig(properties);
                SftpPool sftpPool = new SftpPool(sftpFactory, sftpPoolConfig, sftpAbandonedConfig);
                SftpClient sftpClient = new SftpClient(sftpPool);
                multipleSftpClient.put(name, sftpClient);
            });

            return multipleSftpClient;
        }

        // 单个连接
        SftpFactory sftpFactory = createSftpFactory(sftpClientProperties);
        SftpPoolConfig sftpPoolConfig = createSftpPoolConfig(sftpClientProperties);
        SftpAbandonedConfig sftpAbandonedConfig = createSftpAbandonedConfig(sftpClientProperties);
        SftpPool sftpPool = new SftpPool(sftpFactory, sftpPoolConfig, sftpAbandonedConfig);
        SftpClient sftpClient = new SftpClient(sftpPool);

        return sftpClient;
    }

    public SftpFactory createSftpFactory(SftpClientProperties properties) {
        return new SftpFactory.Builder()
                .host(properties.getHost())
                .port(properties.getPort())
                .username(properties.getUsername())
                .password(properties.getPassword())
                .build();
    }

    public SftpPoolConfig createSftpPoolConfig(SftpClientProperties properties) {
        SftpClientProperties.Pool pool = properties.getPool();
        return new SftpPoolConfig.Builder()
                .maxTotal(pool.getMaxTotal())
                .maxIdle(pool.getMaxIdle())
                .minIdle(pool.getMinIdle())
                .lifo(pool.isLifo())
                .fairness(pool.isFairness())
                .maxWaitMillis(pool.getMaxWaitMillis())
                .minEvictableIdleTimeMillis(pool.getMinEvictableIdleTimeMillis())
                .evictorShutdownTimeoutMillis(pool.getEvictorShutdownTimeoutMillis())
                .softMinEvictableIdleTimeMillis(pool.getSoftMinEvictableIdleTimeMillis())
                .numTestsPerEvictionRun(pool.getNumTestsPerEvictionRun())
                .evictionPolicy(null)
                .evictionPolicyClassName(DefaultEvictionPolicy.class.getName())
                .testOnCreate(pool.isTestOnCreate())
                .testOnBorrow(pool.isTestOnBorrow())
                .testOnReturn(pool.isTestOnReturn())
                .testWhileIdle(pool.isTestWhileIdle())
                .timeBetweenEvictionRunsMillis(pool.getTimeBetweenEvictionRunsMillis())
                .blockWhenExhausted(pool.isBlockWhenExhausted())
                .jmxEnabled(pool.isJmxEnabled())
                .jmxNamePrefix(pool.getJmxNamePrefix())
                .jmxNameBase(pool.getJmxNameBase())
                .build();
    }

    public SftpAbandonedConfig createSftpAbandonedConfig(SftpClientProperties properties) {
        SftpClientProperties.Abandoned abandoned = properties.getAbandoned();
        return new SftpAbandonedConfig.Builder()
                .removeAbandonedOnBorrow(abandoned.isRemoveAbandonedOnBorrow())
                .removeAbandonedOnMaintenance(abandoned.isRemoveAbandonedOnMaintenance())
                .removeAbandonedTimeout(abandoned.getRemoveAbandonedTimeout())
                .logAbandoned(abandoned.isLogAbandoned())
                .requireFullStackTrace(abandoned.isRequireFullStackTrace())
                .logWriter(new PrintWriter(System.out))
                .useUsageTracking(abandoned.isUseUsageTracking())
                .build();
    }
}
