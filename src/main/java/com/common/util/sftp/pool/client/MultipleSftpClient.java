package com.common.util.sftp.pool.client;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author
 * @Describe 多个sftpClient
 * @date
 */
public class MultipleSftpClient implements ISftpClient {

    private ThreadLocal<ISftpClient> threadLocal = new ThreadLocal<>();
    private Map<String, ISftpClient> clientMap = new LinkedHashMap<>();

    @Override
    public void open(Handler handler) {
        ISftpClient client = threadLocal.get();
        if (client == null) {
            throw new RuntimeException("请先选择sftpClient！");
        }
        client.open(handler);
    }

    /**
     * 选择sftpClient
     *
     * @param name sftpClient的名称
     */
    public void choose(String name) {
        threadLocal.remove();
        threadLocal.set(clientMap.get(name));
    }

    /**
     * 加入到客户端缓存中
     *
     * @param name
     * @param client
     */
    public void put(String name, ISftpClient client) {
        clientMap.put(name, client);
    }
}
