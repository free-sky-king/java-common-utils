package com.common.util.sftp.pool;

import com.common.util.sftp.SftpUtil;
import com.jcraft.jsch.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;

import java.util.Properties;

/**
 * @author
 * @Describe sftp工厂
 * @date
 */
@Data
// 自动的给model bean实现equals方法和hashcode方法,callSuper表示调用父类的
@EqualsAndHashCode(callSuper = true)
// 无参构造
@NoArgsConstructor
// 全部参数构造
@AllArgsConstructor
public class SftpFactory extends BasePooledObjectFactory<SftpUtil> {

    private static final String CHANNEL_TYPE = "sftp";
    private static Properties sshConfig = new Properties();
    private String host;
    private int port;
    private String username;
    private String password;

    static {
        sshConfig.put("StrictHostKeyChecking", "no");
    }

    public static class Builder {
        private String host;
        private int port;
        private String username;
        private String password;

        public SftpFactory build() {
            return new SftpFactory(host, port, username, password);
        }

        public Builder host(String host) {
            this.host = host;
            return this;
        }

        public Builder port(int port) {
            this.port = port;
            return this;
        }

        public Builder username(String username) {
            this.username = username;
            return this;
        }

        public Builder password(String password) {
            this.password = password;
            return this;
        }
    }

    /**
     * 创建一个{@link SftpUtil}实例
     * 这个方法必须支持并发多线程调用
     *
     * @return {@link SftpUtil}实例
     */
    @Override
    public SftpUtil create() throws Exception {
        try {
            JSch jsch = new JSch();
            Session sshSession = jsch.getSession(username, host, port);
            sshSession.setPassword(password);
            sshSession.setConfig(sshConfig);
            sshSession.connect();
            ChannelSftp channel = (ChannelSftp) sshSession.openChannel(CHANNEL_TYPE);
            channel.connect();
            return new SftpUtil(channel);
        } catch (JSchException e) {
            throw new RuntimeException("连接sftp失败", e);
        }
    }

    @Override
    public PooledObject<SftpUtil> wrap(SftpUtil sftpUtil) {
        return new DefaultPooledObject<>(sftpUtil);
    }

    /**
     * 销毁对象
     *
     * @param p 对象包装器
     */
    @Override
    public void destroyObject(PooledObject<SftpUtil> p) {
        if (p != null) {
            SftpUtil sftp = p.getObject();
            if (sftp != null) {
                ChannelSftp channelSftp = sftp.getChannelSftp();
                if (channelSftp != null && channelSftp.isConnected()) {
                    channelSftp.disconnect();
                }
            }
        }
    }

    /**
     * 检查连接是否可用
     *
     * @param p 对象包装器
     * @return {@code true} 可用，{@code false} 不可用
     */
    @Override
    public boolean validateObject(PooledObject<SftpUtil> p) {
        if (p != null) {
            SftpUtil sftp = p.getObject();
            if (sftp != null) {
                try {
                    sftp.getChannelSftp().cd("./");
                    return true;
                } catch (SftpException e) {
                    return false;
                }
            }
        }
        return false;
    }
}
